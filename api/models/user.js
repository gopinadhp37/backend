const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    fullname: {type : String, required:true},
    email : {type :String, required:true},
    company : {type : String},
    contact : {type:  Number , required:true},   
    address : {type : String, required:true}
});

module.exports = mongoose.model('User', userSchema)