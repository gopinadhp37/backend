const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('../models/user');

router.get('/', (req, res, next) => {
    User.find()
        .exec()
        .then(users =>{
            console.log(users); 
            res.status(200).json(users);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error})
        })
});

router.post('/', (req, res, next) => {
    const user = new User({
        _id: new mongoose.Types.ObjectId(),
        fullname : req.body.fullname,
        email : req.body.email,
        company : req.body.company,
        contact : req.body.contact,
        address : req.body.address  
    })
    user.save()
    .then(result =>{
        console.log(result);
    })
    res.status(200).json({
        message :'post',
        createdUser : user
    })
});

router.get("/:userId", (req, res, next) => {
    const id = req.params.userId;
    User.findById(id)
        .exec()
        .then(user =>{
            console.log(user);
            if(user){
                res.status(200).json(user);
            }else {
                res.status(404).json({message :"NO Valid Entry Found"});
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error:error});
        })
});

router.patch('/:userId', (req, res, next) => {
   const id = req.params.userId;
   const updateOps = {};
   for(const ops of req.body){
       updateOps[ops.propName] = ops.value;
   }
   User.update({_id :id}, { $set:updateOps})
       .exec()
       .then(result => {
           console.log(result);
           res.status(200).json(result);
       })
       .catch(error =>{
           console.log(error);
           res.status(404).json({error:error});
       })
});

router.delete('/:userId', (req, res, next) => {
    const id = req.params.userId;
    User.remove({_id :id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error});
        })
});


module.exports = router;